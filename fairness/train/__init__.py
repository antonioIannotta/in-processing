from fairness.architecture import create_model, equal_opportunity_loss
from keras.metrics import BinaryAccuracy

def train_model(train_data, sensitive_attributes, epoch_list, val_data):
    fair_model = None
    best_model_accuracy = 0.5
    best_model_loss = None

    input_dim = _return_input_dim(train_data)

    model = create_model(input_dim, 1, 'sigmoid')
    model.compile('adam', loss=equal_opportunity_loss(sensitive_attributes, 1.0), metrics=BinaryAccuracy())
    for epoch in epoch_list:
        print(epoch)
        model.fit(train_data, epochs=epoch)
        loss, accuracy = model.evaluate(val_data)

        print('Loss: ', loss)
        print('Accuracy: ', accuracy)

        if accuracy > best_model_accuracy:
            fair_model = model
            best_model_accuracy = accuracy
            best_model_loss = loss

    return fair_model, best_model_accuracy, best_model_loss
            

def _return_input_dim(train_data):
    for batch in train_data.take(1):
        X_batch, y_batch = batch
        return int(X_batch.shape[-1])