import tensorflow as tf
from tensorflow import keras
from keras import layers
from fairlearn.metrics import equalized_odds_ratio


def create_model(input_dim: int, output_dim: int, output_activation_function: str) -> keras.Sequential:

    model = keras.Sequential()
    model.add(layers.Input(shape=(input_dim,)))
    model.add(layers.Dense(64, activation='relu'))
    model.add(layers.Dense(32, activation='relu'))
    model.add(layers.Dense(output_dim, activation=output_activation_function))

    return model



def equal_opportunity_loss(protected_attributes: list[str], alpha=1.0):


    def loss(y_true, y_pred):
        
        print(y_true)
        print(y_pred)

        loss = tf.keras.losses.binary_crossentropy(y_true, y_pred)
        protected_positive = tf.reduce_mean(tf.boolean_mask(loss, protected_attributes))
        unprotected_positive = tf.reduce_mean(tf.boolean_mask(loss, ~protected_attributes))

        equal_op_loss = tf.abs(protected_positive - unprotected_positive)

        combined_loss = loss + alpha * equal_op_loss

        return combined_loss
    
    return loss