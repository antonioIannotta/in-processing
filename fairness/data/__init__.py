import pandas as pd
from sklearn.preprocessing import LabelEncoder
import tensorflow as tf
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import numpy as np

def _load_data(path: str) -> pd.DataFrame:
    """ this method returns a dataframe starting from the file passed as path

    Args:
        path (str): the file to load

    Returns:
        pd.DataFrame: the dataframe resulting to that dataframe
    """
    df = pd.read_csv(path)
    return df

def _categorical_numerical_converter(df: pd.DataFrame) -> pd.DataFrame:
    """ this method encodes each categorical attributes into the dataframe converting it into a numerical  one

    Args:
        df (pd.DataFrame): the dataframe on which compute the encoding

    Returns:
        pd.DataFrame: the dataframe resulting after the process
    """
    categorical_columns = [column for column in df.columns if df[column].dtype == "O"]
    label_encoder = LabelEncoder()
    for column in categorical_columns:
        df[column] = label_encoder.fit_transform(df[column])

    return df

def _make_attributes_binary(df: pd.DataFrame, attributes: list) -> pd.DataFrame:
    """ this method converts the attributes passed as argument into binary ones

    Args:
        df (pd.DataFrame): the datafrane on which perform the computation
        attributes (list): the attributes to convert

    Returns:
        pd.DataFrame: the dataframe with the specified attributes converted into binary ones
    """
    for column in attributes:
        mean_value = df[column].mean()
        binary_array = []
        for val in df[column].values:
            if val <= mean_value:
                binary_array.append(0)
            else:
                binary_array.append(1)
        df[column] = binary_array

    return df

#TODO: TRANSFORM DATA IN TENSORFLOW FORMAT

def _dataframe_to_dataset(dataframe: pd.DataFrame):
    return tf.data.Dataset.from_tensor_slices(dict(dataframe))


def return_model_data(path: str, attributes: list[str], output_column: str, protected_attributes: list[str]):

    dataframe = _load_data(path)
    dataframe = _categorical_numerical_converter(dataframe)
    dataframe = _make_attributes_binary(dataframe, attributes)

    sensitive_attributes_bool_array = []
    for column in dataframe.columns:
        if column in protected_attributes:
            sensitive_attributes_bool_array.append(1)
        else:
            sensitive_attributes_bool_array.append(0)

    X = dataframe.drop(columns=[output_column], inplace=False)
    y = dataframe[output_column]

    X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=0.2, random_state=42)

    scaler = StandardScaler()
    X_train = scaler.fit_transform(X_train)
    X_val = scaler.transform(X_val)

    train_data = tf.data.Dataset.from_tensor_slices((X_train, y_train.astype(int)))
    val_data = tf.data.Dataset.from_tensor_slices((X_val, y_val.astype(int)))

    batch_size = 32
    train_data = train_data.batch(batch_size)
    val_data = val_data.batch(batch_size)

    return train_data, val_data, np.array(sensitive_attributes_bool_array)